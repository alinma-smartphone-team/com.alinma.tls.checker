// check is tls 1.2 supported or not

var exec = require('cordova/exec');

function TlsCheckerPlugin() { }

TlsCheckerPlugin.prototype.isTLS12Supported = function (successCallback, failureCallback) {
	exec(successCallback, failureCallback, 'TlsCheckerPlugin', 'IsTLS12Supported', []);
};

// module.exports = new TlsCheckerPlugin();

// Installation constructor that binds TlsCheckerPlugin to window
TlsCheckerPlugin.install = function () {
	if (!window.plugins) {
		window.plugins = {};
	}
	window.plugins.TlsCheckerPlugin = new TlsCheckerPlugin();
	return window.plugins.TlsCheckerPlugin;
};
cordova.addConstructor(TlsCheckerPlugin.install);

