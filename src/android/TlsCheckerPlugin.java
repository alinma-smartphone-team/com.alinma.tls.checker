package com.alinma.tls;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class TlsCheckerPlugin extends CordovaPlugin {
    //for message sending
	public final String ACTION_SEND_SMS = "SendSMS";

	//for checking tls 1.2 suported
	public final String ACTION_IS_TLS_1_2_SUPPORTED="IsTLS12Supported";
	
    //for message receiving
    public final String ACTION_HAS_SMS_POSSIBILITY = "HasSMSPossibility";
    public final String ACTION_RECEIVE_SMS = "StartReception";
    public final String ACTION_STOP_RECEIVE_SMS = "StopReception";

//    private CallbackContext callback_receive;
//    private SmsReceiver smsReceiver = null;
//    private boolean isReceiving = false;

	@Override
	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
		if(action.equals(ACTION_IS_TLS_1_2_SUPPORTED)){
			
//			Activity ctx=this.cordova.getActivity();
//			if(ctx.getPackageManager.hasS)
			// check tls 1.2 version supportd
			// if true 
			boolean value = this.isTls1_2Supported();
			Log.d("BBB", String.valueOf(this.isTls1_2Supported()));
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK,value));
			
			//else
			//callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK),false);
			
			return true;
		}
		/*if(action.equals(ACTION_HAS_SMS_POSSIBILITY)){
            Activity ctx = this.cordova.getActivity();
            if(ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)){
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, true));
            } else {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, false));
            }
            return true;
        }else if (action.equals(ACTION_RECEIVE_SMS)) {

            // if already receiving (this case can happen if the startReception is called
            // several times
            if(this.isReceiving) {
                // close the already opened callback ...
                PluginResult pluginResult = new PluginResult(
                        PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(false);
                this.callback_receive.sendPluginResult(pluginResult);

                // ... before registering a new one to the sms receiver
            }
            this.isReceiving = true;

            if(this.smsReceiver == null) {
                this.smsReceiver = new SmsReceiver();
                IntentFilter fp = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
                fp.setPriority(1000);
                // fp.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
                this.cordova.getActivity().registerReceiver(this.smsReceiver, fp);
            }

            this.smsReceiver.startReceiving(callbackContext);

            PluginResult pluginResult = new PluginResult(
                    PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            this.callback_receive = callbackContext;

            return true;
        }else if(action.equals(ACTION_STOP_RECEIVE_SMS)) {

            if(this.smsReceiver != null) {
                smsReceiver.stopReceiving();
            }

            this.isReceiving = false;

            // 1. Stop the receiving context
            PluginResult pluginResult = new PluginResult(
                    PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(false);
            this.callback_receive.sendPluginResult(pluginResult);

            // 2. Send result for the current context
            pluginResult = new PluginResult(
                    PluginResult.Status.OK);
            callbackContext.sendPluginResult(pluginResult);

            return true;
        }*/
		return false;
	}
    
    public boolean isTls1_2Supported(){
        boolean isSupported=false;
        SSLSocketFactory sslSocketFactory=null;
		SSLContext sslContext =null;
		SSLSocket sslSocket =null;
		try {
			 sslContext =SSLContext.getInstance("Default");
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sslSocketFactory = sslContext.getSocketFactory();
		try {
			sslSocket =(SSLSocket) sslSocketFactory.createSocket();}
		catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        	
		}
		
		String [] protcols = sslSocket.getEnabledProtocols();

		
		
	

		for (int i = 0; i < protcols.length; i++) {
			Log.d("Plugin", "$$$$$$$$$$$$$$$$$$$$$$$ ::: "+protcols[i]);
            if (protcols[i].equals("TLSv1.2")){
                isSupported=true;
            }
		}
		Log.d("AAA", String.valueOf(isSupported));
        return isSupported;
    }
}
